import fetch from "node-fetch"

export module SunriseSunset {
  export type Response =
    | { status: "OK"; results: RawResults }
    | { status: "INVALID_REQUEST" | "INVALID_DATE" | "UNKNOWN_ERROR"; results: "" }

  export interface RawResults {
    sunrise: string
    sunset: string
    solar_noon: string
    day_length: number
    civil_twilight_begin: string
    civil_twilight_end: string
    nautical_twilight_begin: string
    nautical_twilight_end: string
    astronomical_twilight_begin: string
    astronomical_twilight_end: string
  }

  export interface Results {
    /** Time at which the sun rose. */
    sunrise: Date
    /** Time at which the sun set. */
    sunset: Date
    /** Length of the day, in seconds. */
    dayLength: number
  }

  export interface Options {
    /** Latitude in decimal degrees. Required. */
    lat: number
    /** Longitude in decimal degrees. Required. */
    lng: number
    /** Date. Optional, defaults to the current date. */
    date?: Date
  }
}

export const SunriseSunset = {
  /** Make a request to the API and return the JSON response data. */
  async rawRequest<T>(query: Record<string, string | number> = {}) {
    const params = new URLSearchParams()
    for (const k in query) params.set(k, String(query[k]))

    const response = await fetch("https://api.sunrise-sunset.org/json?" + params.toString())

    if (!response.ok) {
      throw new Error(`API responded with status: ${response.status} ${response.statusText}`)
    }

    return response.json() as Promise<T>
  },

  /** Fetch sunset/sunrise results for the given options. */
  async getResults(options: SunriseSunset.Options): Promise<SunriseSunset.Results> {
    const { lat, lng, date = new Date() } = options
    const query = { lat, lng, date: formatDate(date), formatted: 0 }

    const response = await SunriseSunset.rawRequest<SunriseSunset.Response>(query)

    if (response.status !== "OK") {
      throw new Error(`API responded with error: ${response.status}`)
    }

    return {
      sunrise: new Date(response.results.sunrise),
      sunset: new Date(response.results.sunset),
      dayLength: response.results.day_length,
    }
  },
}

/** Given a Date object, format it as YYYY-MM-DD. */
function formatDate(value: Date) {
  const [date, _time] = value.toISOString().split("T")
  return date
}
