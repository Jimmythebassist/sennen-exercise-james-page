import readline from "readline"
import { SunriseSunset } from "./SunriseSunset"

interface Options {
  /** The number of random results to fetch in total. */
  totalCount: number
  /** The maximum number of results to fetch in parallel. */
  batchSize: number
  /** Whether or not to log progress STDOUT. */
  verbose?: boolean
}

/**
 * Randomly fetch a number of results from the sunrise-sunset API, then return the one with the
 * earliest sunrise.
 */
export async function findEarliestSunrise(options: Options) {
  const { totalCount, batchSize, verbose = false } = options
  const batchCount = Math.ceil(totalCount / batchSize)

  let minimum: SunriseSunset.Results | undefined
  for (let i = 0; i < batchCount; i++) {
    const count = Math.min(batchSize, totalCount - batchSize * i)

    if (verbose) writeOver(`Fetching batch: ${i + 1}/${batchCount}`)
    const results = await getRandomResults(count)

    for (const result of results) {
      if (!minimum || result.sunrise < minimum.sunrise) {
        minimum = result
      }
    }
  }

  if (verbose) writeOver("")

  return minimum!
}

/** In parallel, fetch a number of random results from the sunrise-sunset API. */
function getRandomResults(count: number) {
  const promises = Array<Promise<SunriseSunset.Results>>(count)
  for (let i = 0; i < count; i++) {
    const { lat, lng } = getRandomCoordinates()
    promises[i] = SunriseSunset.getResults({ lat, lng })
  }
  return Promise.all(promises)
}

/**
 * Get a random set of coordinates from around the world, excluding Antarctica.
 * Antarctica is excluded as many days there will have no sunrise.
 */
function getRandomCoordinates() {
  return {
    // Narrowed from (-90, 90) to exclude Antarctica.
    lat: getRandomNumberInRange(-60, 85),
    lng: getRandomNumberInRange(-180, 180),
  }
}

/** Get a random decimal number between the given minimum and maximum. */
function getRandomNumberInRange(minimum: number, maximum: number) {
  return minimum + Math.random() * (maximum - minimum)
}

/** Write into STDOUT, erasing any content on the current line. */
export function writeOver(message: string) {
  readline.clearLine(process.stdout, 0)
  readline.cursorTo(process.stdout, 0)
  process.stdout.write(message)
}
