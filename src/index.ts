import { findEarliestSunrise } from "./findEarliestSunrise"

async function run() {
  const minimum = await findEarliestSunrise({ totalCount: 100, batchSize: 5, verbose: true })
  const length = new Date(minimum.dayLength * 1000)

  console.log("Earliest Sunrise:")
  console.log("  Sunrise:", minimum.sunrise.toUTCString())
  console.log("  Sunset:", minimum.sunset.toUTCString())
  console.log("  Day Length:", length.getHours(), "hours", length.getMinutes(), "minutes")
}

run()
  .then(() => process.exit(0))
  .catch((e) => {
    console.error(e)
    process.exit(1)
  })
